# vim: set expandtab ts=4 sw=4:

# This file is part of the python-megdata package
# For copyright and redistribution details, please see the COPYING file

from .support import megdatatestpath, array_assert
