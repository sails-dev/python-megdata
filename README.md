python-megdata
==============

This library allows binary read access to different filetypes used by MEG
machines.  At the moment, the library supports 4D (BTI), CTF and YI
(FMT-01 MEGHDF5) datasets.

We would like to encourage patches to support other file-types as well as
documentation patches and support for writing out modified files.


Dependencies
============

The only real requirements to use python-megdata are numpy and scipy:

    python-numpy >= 1.12.0
    python-scipy >= 0.18.0

If you want to use the MEGHDF file format, you will require the h5py module:

    python-h5py >= 2.7.0


Usage and Documentation
=======================

At the moment, there is little documentation.  Your best starting point are
the example scripts in the bin/ directory


License
=======

This project is currently licensed under the GNU General Public Licence 2.0 or
higher.  For the avoidance of doubt, the authors intend that if the code is
imported into a project which is not licensed under a GPL 2.0 compatible
license, an alternative license arranged must be acquired.  For alternative
license arrangements such as use in commercial products for which you do not
which to release the source code under a GPL compatible license, please contact
the authors.  The authors are also available for consultancy regarding this
toolbox and associated methods.

