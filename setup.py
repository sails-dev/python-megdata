#!/usr/bin/python

from setuptools import setup

scripts = ['megdataBTIConfigFile',
           'megdataBTICreateM4D',
           'megdataBTIElectrodeFile',
           'megdataBTIHSFile',
           'megdataBTIPDF',
           'megdataBTIToMEGHDF',
           'megdataCTFDataset',
           'megdataCTFRes4',
           ]


setup(
    name = 'megdata',
    description = 'Python MEG data readers',

    version = '1.0.3',

    author = 'Mark Hymers',
    author_email = 'ynic-devel@ynic.york.ac.uk',
    url = 'https://vcs.ynic.york.ac.uk/naf/python-megdata',

    long_description = '''
Python MEG readers for various file types
''',

    license = 'GNU GPL v2 or later',

    classifiers = [
        'Development Status :: 5 - Production/Stable',
        'Environment :: Console',
        'Intended Audience :: Science/Research',
        'License :: OSI Approved :: GNU General Public License v2 or later (GPLv2+)',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Topic :: Scientific/Engineering :: Bio-Informatics',
        'Topic :: Scientific/Engineering :: Medical Science Apps.',
    ],

    keywords = ['MEG', 'magnetoencephalography'],

    packages = ['megdata', 'megdata.test', 'megdata.tests'],
    scripts = ['bin/%s' % s for s in scripts],

    install_requires = ['numpy>=1.12', 'scipy >= 0.18'],

    extras_require = {
        'meghdf': ['h5py>=2.7.0']
    },

)

