# vim: set noexpandtab ts=4 sw=4:

# This Makefile is only for use by developers when wanting to
# test building of docs or tests locally

# There must be a nicer way to do this
PYTHONARCH=$(shell utils/pythonarch)

DEVELVERSION ?= 0.DEVEL.$(shell date +%Y%m%d%H%M)
RELEASEVERSION ?= $(DEVELVERSION)

all:
	python setup.py build

doc: doc-html

doc-html: all sphinx-html

sphinx-html: all
	sphinx-build -D version=$(DEVELVERSION) -D release=$(RELEASEVERSION) -a doc docgen/html

sphinx-pdf: all
	PAPER=A4 sphinx-build -D version=$(DEVELVERSION) -D release=$(RELEASEVERSION) -b latex -a doc docgen/latex
	cd docgen/latex && $(MAKE)

clean:
	python setup.py clean
	rm -fr docgen build

# We share the testdata repository with NAF
# TODO: We will include our own (small) test data in future
test: all
	cd build && PYTHONPATH=lib.$(PYTHONARCH)/:$${PYTHONPATH} MEGDATATEST=../../naf-testdata nosetests -c ../nose.cfg lib.$(PYTHONARCH)/megdata

testv: all
	cd build && PYTHONPATH=lib.$(PYTHONARCH)/:$${PYTHONPATH} MEGDATATEST=../../naf-testdata nosetests -c ../nose.cfg -v lib.$(PYTHONARCH)/megdata

install:
	python setup.py install --prefix=$(DESTDIR)/usr
	mkdir -p $(DESTDIR)/usr/share/doc/python-megdata
	cp -a docgen/html $(DESTDIR)/usr/share/doc/python-megdata

build_dist:
	python3 setup.py sdist
	python3 setup.py bdist_wheel --universal

.PHONY: clean doc doc-html all test testv pythonarch build_dist
