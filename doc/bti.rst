======================
BTI (4D) Readers
======================

.. toctree::
   :maxdepth: 1

###########################
Config File Reader
###########################

This class allows reading both system- and datafile- config files.  For example: ::

  >>> from megdata import BTIConfigFile
  >>> c = BTIConfigFile.from_file('/path/to/config')
  >>> print c.hdr
  <BTIConfigHeader
  Version:           1
  Sitename:          YNiC
  DAP Hostname:      btiwd01
  System Type:       7
  System Options:    983041
  Supply Freq:       50
  Total Chans:       386
  System Fixed Gain: 1.000000e+00
  Volts Per Bit:     8.630400e-05
  Total Sensors:     1
  Total User Blocks: 15
  Next Der Chan No:  1000
  Checksum:          4294965157
  >

.. autoclass:: megdata.BTIConfigFile

###########################
PDF (Data) Reader
###########################

This class allows reading actual MEG Data files.  For example: ::

  >>> from megdata import BTIPDF
  >>> p = BTIPDF.from_file('/path/to/c,rfDC')
  >>> print p.hdr
  <BTIPDFHeaderData
    Version:          1
    File Type:        Bts
    Data Format:      1
    Acq Mode:         1
    Total Epochs:     1
    Input Epochs:     0
    Total Events:     1
    Total Fix Events: 1
    Sample Period:    0.000246
    X-Axis Label:     s
    Total Processes:  2
    Total Chans:      274
    Checksum:         -932
    Total Ed Classes: 0
    Total Assc Files: 0
    Last File Index:  0
    Timestamp:        Thu Jan  1 01:00:00 1970 (0)
  >
  >>> # Read the 0th channel, data points 0-100 (python-style indexing)
  >>> # Note that this routine does not deal with UPB, Scale or Gain
  >>> print p.read_raw_data( (0, 100), 0 )
  [0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
   0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
   0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0]

.. autoclass:: megdata.BTIPDF

###########################
HS (Headshape) Reader
###########################

This reader allows reading headshape files.  For example: ::

  >>> from megdata import BTIHSFile
  >>> h = BTIHSFile.from_file('/path/to/hs_file')
  >>> print h
  <BTIHSFile
    Version:        1
    Timestamp:      Wed Oct 13 09:55:04 2010 (1286960104)
    Num dig points: 1804
    Index points:   [[  4.23827864e-03   6.93309779e-02   1.08420217e-18]
                     [ -4.23827864e-03  -6.93309779e-02   1.88651178e-17]
                     [  5.95518891e-02  -1.95156391e-18   3.46944695e-18]
                     [  5.93076484e-02   4.26605862e-02   5.73207181e-02]
                     [  6.67982588e-02  -3.57254031e-02   5.81618062e-02]]
    Dig points:     [[ 0.0386441   0.05810324  0.05017209]
                     [ 0.03834943  0.05906723  0.04637948]
                     [ 0.03790937  0.0594297   0.04298317]
                     ...,
                     [ 0.04492167  0.01882461  0.09876295]
                     [ 0.04673992  0.01667053  0.09805308]
                     [ 0.04943265  0.01393149  0.09601916]]
  >

.. autoclass:: megdata.BTIHSFile

###########################
Electrode file Reader
###########################

This reader allows reading electrode files (for example the ``.el`` files which
can be found in ``/home/whsbti/headshapes`` on the acquisition machine).  For
example: ::

  >>> from megdata import BTIElectrodeFile
  >>> e = BTIElectrodeFile.from_file('/path/to/elfile')
  >>> print e
  <BTIElectrodeFile
    <BTIElectrode
      Location:       [[  3.54677851e-03   7.49790328e-02  -1.56396163e-17]]
      Label:          L
      State Str:
      State:          Not Collected (0)
    >
    <BTIElectrode
      Location:       [[ -3.54677851e-03  -7.49790328e-02   1.65069781e-17]]
      Label:          R
      State Str:
      State:          Not Collected (0)
    >
    <BTIElectrode
      Location:       [[ 0.06071989  0.          0.        ]]
      Label:          N
      State Str:
      State:          Not Collected (0)
    >
    ...
  >

.. autoclass:: megdata.BTIElectrodeFile
