======================
CTF Readers
======================

.. toctree::
   :maxdepth: 1

#############################
Resource file (.res4) reader
#############################

This class allows reading a ``.res4 file``  For example: ::

  >>> from megdata import CTFRes4File
  >>> r = CTFRes4File.from_file('/path/to/dataset.ds/dataset.res4')
  >>> print r
  <CTFRes4File
    Version:           MEG42RS
    App Name:
    Data Origin:
    Data Description
    No Trials Avg:     0
    Data Time:         12:25
    Data Date          15-Dec-2010
    No Samples:        12000
    No Channels:       367
    Sample Rate:       600.000000
    Epoch Time:        600.000000
    No Trials:         30
    No Trials Done:    7680
    Pre Trigger Pts:   0
    No Trials Display: 0
    Save Trials:       0
    Primary Trigger:   0
    Trig Pol Mask:     0x00000000
    Trigger Mode:      0
    Acc/Rej Flag:      0
    Run Time Display:  0
    Zero Head Flag:    0
    Artifact Mode:     0
    Run Name:
    Run Title:         Motor
    Instruments:
    Collect Desc:
    Subject ID:        0000
    Operator:          XX
    Sensor Filename:
    RDLen:             1
    Run Description:
    Num Filters:       0
    Channel Names:
    <CTFChannel
      Name:              UPPT002
      Channel Type:      11 (Stimulus channel)
      Origin Run Num:    0
      Coil Shape:        0
      Gain:              1.000000e+00
      Q Gain:            1.000000e+00
      IO Gain:           1.000000e+00
      IO Offset:         0.000000e+00
      Grad Order No:     0
      Num Coils:         0
    >
   ...
   >

.. autoclass:: megdata.CTFRes4File

###########################
CTF Dataset Reader
###########################

This class wraps the CTFRes4File reader along with methods for reading the raw data
from the ``.meg4`` file.  For example: ::

  >>> from megdata import CTFDataset
  >>> r = CTFDataset.from_file('/path/to/dataset.ds/dataset.res4')
  >>> print r
  <CTFDataset
    Resource Filename: /path/to/dataset.ds/dataset.res4
    Data Filename:     /path/to/dataset.ds/dataset.meg4
    <CTFRes4File
      Version:           MEG42RS
      App Name:
  ...
  >
  >>> # Have a look at the 0th indexed channel
  >>> print r.res.channels[0]
  <CTFChannel
    Name:              UPPT002
    Channel Type:      11 (Stimulus channel)
    Origin Run Num:    0
    Coil Shape:        0
    Gain:              1.000000e+00
    Q Gain:            1.000000e+00
    IO Gain:           1.000000e+00
    IO Offset:         0.000000e+00
    Grad Order No:     0
    Num Coils:         0
  >
  >>> # And an MEG channel
  >>> print r.res.channels[40]
  <CTFChannel
    Name:              MLC25-4121
    Channel Type:      5 (1st order gradiometer)
    Origin Run Num:    0
    Coil Shape:        0
    Gain:              3.063203e+09
    Q Gain:            1.048576e+06
    IO Gain:           1.000000e+00
    IO Offset:         0.000000e+00
    Grad Order No:     0
    Num Coils:         2
      Coil 0:
      <CTFCoil
        Position:          [[ -7.662    4.6579 -13.0736]]
        Orientation:       [[ 0.68166019 -0.53506453 -0.49904443]]
        Number of Turns:   2
        Area:              2.547235
      >
      Coil 0 (HD):
      <CTFCoil
        Position:          [[  1.74458366   8.23868286  14.65840882]]
        Orientation:       [[ -8.72764189e-05  -8.40282134e-01  -5.42149360e-01]]
        Number of Turns:   0
        Area:              0.000000
      >
      Coil 1:
      <CTFCoil
        Position:          [[-11.07116664   7.33390217 -10.57774407]]
        Orientation:       [[-0.68166019  0.53506453  0.49904443]]
        Number of Turns:   2
        Area:              2.547235
      >
      Coil 1 (HD):
      <CTFCoil
        Position:          [[  1.74502015  12.44116069  17.36984415]]
        Orientation:       [[  8.72765262e-05   8.40282133e-01   5.42149360e-01]]
        Number of Turns:   0
        Area:              0.000000
      >
  >
  >>> # Read some of the Stimulus channel in
  >>> print r.read_raw_data( (0, 100), 0 )
  [2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2
   2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2
   2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2]
  >>> # Read the MEG channels in - note that this is raw data
  >>> # and has not been scaled by the relevant factors
  >>> print r.read_raw_data( (0, 100), r.meg_indices ).shape
  (100, 271)
  >>> # Read all channels in - again, raw data
  >>> print r.read_raw_data( (0, 100), None ).shape
  (100, 367)
  >>> # The factors can be found in the Dataset, one per channel
  >>> print r.scales.shape
  (1, 367)


.. autoclass:: megdata.CTFDataset

#########################################
CTF Polhemus Reader
#########################################

This class allows reading Polhemus files often used with CTF data.  For example: ::

  >>> from megdata import CTFPolhemus
  >>> p = megdata.CTFPolhemus.from_file('/path/to/file.pos')
  >>> # Note that the positions in this file are in cm
  >>> print p
  <CTFPolhemus
    Num Points:        547
    Points:            [[  9.74728234   1.80959068   9.79442306]
   [  9.47202054  -0.42359642  10.73671003]
   [ 10.25089835  -2.12493168   8.69798668]
   ...,
   [  9.86289206  -0.16462131   0.02502681]
   [  9.83627662  -0.16830425   0.05088059]
   [  9.85103661  -0.15559661   0.02702646]]
    Num Fiducials      3
    Fiducial Names:
      nasion
      left
      right
    Fiducials:         [[  9.60420380e+00   8.00683304e-16   4.15899953e-16]
   [  7.13538796e-02   6.38156998e+00   1.27068495e-16]
   [ -7.13538796e-02  -6.38156998e+00   1.12106505e-16]]
  >

