#################################
Python MEGData Manual
#################################

:Release: |version|
:Date: |today|

Introduction
------------

The documentation for this module is very rough and ready right now (as is the
code itself).  The best thing to do is read the example scripts.

Class Reference
---------------

.. toctree::
   :maxdepth: 1

   bti
   ctf

Indices
-------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

