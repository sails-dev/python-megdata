
import os
import sys
from commands import getstatusoutput

sys.path.append(os.path.abspath('.'))
# Eww...
status, path = getstatusoutput('../utils/buildpath')
if status != 0:
    raise StandardError("Error calling buildpath command")
if not os.path.isdir(path):
    raise StandardError("Cannot find MEGData install directory %s" % path)

sys.path.append(path)

extensions = ['sphinx.ext.autodoc', \
              'sphinx.ext.todo', \
              'sphinx.ext.graphviz', \
             ]

project = 'python-megdata'
source_suffix = '.rst'
master_doc = 'index'
latex_documents = [('index', 'index.tex', 'Python MEGData Documentation', 'Mark Hymers', 'manual', True)]
autodoc_default_flags = ['members', 'undoc-members', 'show-inheritance']
